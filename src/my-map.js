function initMap() {

    var map = new google.maps.Map(document.getElementsByName('map')[0], {
    zoom: 8,
    center: {lat: -34.397, lng: 150.644}
  });

  var geocoder = new google.maps.Geocoder();
  geocodeAddress(geocoder, map);

}

function geocodeAddress(geocoder, resultsMap) {

        var address = "María Tubau, 10, Madrid"
        // var address = document.getElementsByName("location")[0].value;
/* "mi variable de localidad"*/

        console.log(address);
        geocoder.geocode({'address': address}, function(results, status) {
          if (status === 'OK') {
            resultsMap.setCenter(results[0].geometry.location);
            var marker = new google.maps.Marker({
              map: resultsMap,
              position: results[0].geometry.location
            });
          } else {
            alert('Geocode was not successful for the following reason: ' + status);
          }
        });
  }
